from django.urls import path 
from . import views

urlpatterns = [
    path('login/', views.login, name='login_view'),
    path('signup/', views.signup, name='signup_view'),
    path('profile/edit/', views.settings, name='settings_for_user'),
    path('logout/', views.logout, name='logout_view'),
]
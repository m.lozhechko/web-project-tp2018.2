from django.shortcuts import HttpResponse, redirect
from django.template import loader

from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout

from django.contrib.auth.models import User

from django.http import HttpResponseRedirect
from django.urls import reverse

from .forms import LoginForm, SignupForm

from django.core.exceptions import ValidationError
from django.core.validators import validate_email

from django.contrib.auth.models import User

base_info = {
    'popular_users': {'user1', 'user2', 'user3'},
    'popular_tags': {'Django', 'C', 'Python'}
}

def login(request):
    if request.user.is_authenticated:
        return redirect('/') 

    form = LoginForm(data=request.POST)
    message = 'Login'
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                lasturl = request.session.get('lasturl')
                if lasturl is not None:
                    return redirect(lasturl)
                else: 
                    return redirect('/')     
        message = 'Wrong input data!'

    request.session['lasturl'] = request.META['HTTP_REFERER']
    template = loader.get_template('user/default_form.html')
    return HttpResponse(template.render({
        'main_title' : message, 
        'base_info' : base_info,
        'form' : form, 
        'action' : 'login'}, 
    request))

def signup(request):
    if request.user.is_authenticated:
        return redirect('/') 

    form = SignupForm(data=request.POST)
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            print(form.cleaned_data)
            user = User.objects.create_user(username, email, password)
            user.first_name = form.cleaned_data['nickname']
            user.save()
            auth_login(request, user)
            return redirect('/')

    message = 'Signup page'
    template = loader.get_template('user/default_form.html')
    return HttpResponse(template.render({
        'main_title' : "Sign up", 
        'base_info' : base_info,
        'form' : form,
        'action' : 'signup'}, 
    request))    

# Logout user
def logout(request):
    auth_logout(request)
    return redirect('/')

def settings(request):
    if not request.user.is_authenticated:
        return redirect('/') 

    message = 'Settings page'
    form = SignupForm(data=request.POST)
    if request.method == 'POST':
        if form.is_valid():
            user = request.user
            user.username = form.cleaned_data['username']
            user.email = form.cleaned_data['email']
            user.password = form.cleaned_data['password']
            user.password_rep = form.cleaned_data['password_rep']
            user.first_name = form.cleaned_data['nickname']
            user.save()
        else:
            message = 'Wrong input data!'

    template = loader.get_template('user/default_form.html')
    return HttpResponse(template.render({
        'main_title' : message, 
        'base_info' : base_info,
        'form' : form,
        'action' : 'profile/edit'}, 
    request))    
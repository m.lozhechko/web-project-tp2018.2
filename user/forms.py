from django import forms
from django.contrib.auth.models import User
from django.core.validators import validate_email

class LoginForm(forms.Form):
    username = forms.CharField(max_length=100)
    password = forms.CharField(widget=forms.PasswordInput())
    
class SignupForm(forms.Form):
    username = forms.CharField(max_length=100, required=True)
    nickname = forms.CharField(max_length=100, required=True)
    email = forms.CharField(max_length=100, required=True)
    password_rep = forms.CharField(widget=forms.PasswordInput(), required=True)
    password = forms.CharField(widget=forms.PasswordInput(), required=True)

    class Meta:
        model = User
        fields = '__all__'

    def clean_username(self):
        cleaned_data = super().clean()
        username = cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("User is already exists")
        return username

    def clean_email(self):
        cleaned_data = super().clean()
        validate_email(cleaned_data['email'])
        return cleaned_data['email']

    def clean_password(self):
        cleaned_data = super().clean()
        password = cleaned_data.get('password')
        password2 = cleaned_data.get('password_rep')
        if (password != password2):
            raise forms.ValidationError("Passwords are not the same")
        return password
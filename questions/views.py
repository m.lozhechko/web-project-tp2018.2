from django.shortcuts import HttpResponse, redirect
from django.template import loader

from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.core.paginator import Paginator

from .forms import QuestionForm
from questions.models import Question

import datetime

base_info = {
    'popular_users': {'user1', 'user2', 'user3'},
    'popular_tags': {'Django', 'C', 'Python'}
}

def get_posts():
    posts = []
    for i in range(1, 15):
        posts.append({
            'title': 'title' + str(i),
            'id': i,
            'text': 'text' + str(i),
            'rating': str(5 + i),
            'author': 'user' + str(i),
            'date': '10.10.10',
            'tags': {'abra', 'ka', 'dabra'}
    })
    return posts

def get_answers(post_id = 0):
    answers = []
    for i in range(1, 6):
        answers.append({
            'text': 'text' + str(i),
            'rating': str(5 + i),
            'author': 'user' + str(i),
            'date': '10.10.10',
    })
    return answers


def index(request):
    template = loader.get_template('hot_questions.html')
    posts = get_posts()

    paginator = Paginator(posts, 5)
    posts = paginator.get_page(request.GET.get('page'))
    
    return HttpResponse(
        template.render({
            'main_title' : 'The hottest questions in the wild west',
            'relevant_posts' : posts,
            'base_info' : base_info
        }, 
        request))

def get_single_quesiton(request, question_id):
    posts = get_posts()
    answers = get_answers()
    
    template = loader.get_template('get_single_question.html')
    context = {
        'main_title' : 'Question: ' + str(question_id),
        'post' : posts,
        'base_info' : base_info,
        'answers' : answers
    }

    return HttpResponse(template.render(context, request))

def create_single_question(request):
    form = QuestionForm(data=request.POST)
    template = loader.get_template('user/default_form.html')
    message = "Create question"
    if request.method == 'POST':
        if form.is_valid():
            title = form.cleaned_data['question_title']
            text = form.cleaned_data['question_text']
            tag = form.cleaned_data['tag1_title']
            
            question = Question()
            question.question_text = text
            question.question_title = title
            question.tag1_title = tag

            #todo
            id = 1
            question.question_id = id
            question.question_rating = 0
            question.question_date = datetime.date.today()

            question.save()
            return redirect('/')     
        message = 'Wrong input data!'
    return HttpResponse(template.render({
        'main_title' : message, 
        'base_info' : base_info,
        'form' : form,
        'action' : 'ask'}, 
    request))    

def get_questions_by_tag(request, tag):
    posts = get_posts()

    template = loader.get_template('get_questions_by_tag.html')
    return HttpResponse(template.render({
            'main_title' : 'Tag: ' + str(tag),
            'relevant_posts' : posts,
            'base_info' : base_info
        },
    request))

# Something went wrong case
def least_pages(request):
    template = loader.get_template('server_message.html')
    args = {
        'main_title' : 'Page is not exists'
    }

    return HttpResponse(template.render(args, request))
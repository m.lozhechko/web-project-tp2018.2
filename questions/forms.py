from django.forms import ModelForm
from django.db import models
from questions.models import Question

class QuestionForm(ModelForm):
    class Meta:
        model = Question
        fields = ['question_title', 'question_text', 'tag1_title']
from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.index),
    path('ask/', views.create_single_question, name='create_single_question'),
    path('question/<int:question_id>/', views.get_single_quesiton, name='get_single_question'),
    path('tag/<tag>/', views.get_questions_by_tag, name='tag_view'),
    re_path(r'', views.least_pages)
]
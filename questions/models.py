from django.db import models

class Question(models.Model):
    question_title = models.CharField(max_length = 100)
    question_text = models.CharField(max_length = 500)
    question_id = models.IntegerField()
    question_rating = models.IntegerField()
    question_date = models.DateField()
    tag1_title = models.CharField(max_length = 50)
